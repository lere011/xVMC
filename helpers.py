import os
import math
import numpy as np
import numpy.typing as npt
import pandas as pd
import configparser
from functools import cache
from typing import Union, Any
from structures import Edit, Experiment
from collections.abc import Iterable
import matplotlib.pyplot as plt  # type: ignore


@cache
def get_data(training_dir: str) -> npt.NDArray[np.int32]:
    """This is used to retrieve training data from files.

    Parameters
    ----------
    training_dir: a directory containing at least one .txt file
    """
    files = [f"{training_dir}/{f}" for f in os.listdir(training_dir)]
    data = [np.loadtxt(f, dtype=np.int32) for f in files]
    dataset = np.vstack(data)
    return dataset


@cache
def read_config(config_file: str) -> configparser.ConfigParser:
    if os.path.exists(config_file) and os.path.isfile(config_file):
        config = configparser.ConfigParser()
        config.read(config_file)
        return config
    raise TypeError("File does not exist")


@cache
def get_all_interactions(n: int) -> pd.DataFrame:
    elements = list(range(n * n))
    coordinates = [(i, j) for i in range(n) for j in range(n)]
    my_dict = {k: v for k, v in zip(coordinates, elements)}

    unique_pairs = [
        (coordinates[i], coordinates[j])
        for i in range(len(coordinates))
        for j in range(i + 1, len(coordinates))
    ]
    df = pd.DataFrame(unique_pairs, columns=["Point 1", "Point 2"])

    df["Distance"] = df.apply(
        lambda x: math.sqrt(
            abs(x["Point 1"][0] - x["Point 2"][0]) ** 2
            + abs(x["Point 1"][1] - x["Point 2"][1]) ** 2
        ),
        axis=1,
    )
    df["Divider"] = df.apply(lambda row: row["Distance"] ** 6, axis=1)

    df["Multiplier"] = df.apply(lambda row: 1 / row["Divider"], axis=1)

    df["Index 1"] = df.apply(
        lambda row: n * row["Point 1"][0] + row["Point 1"][1], axis=1
    )

    df["Index 2"] = df.apply(
        lambda row: n * row["Point 2"][0] + row["Point 2"][1], axis=1
    )

    return df


def line_plot(
    x_values: Iterable[float],
    /,
    abs_value: Union[int, float, None] = None,
    xlabel: str = "Step",
    ylabel: str = "$\\langle H \\rangle/N$",
    title: str = "Energy Density Over Epochs for Data-Baased Training",
    fig_name: str = "",
) -> None:
    fig = plt.figure(1, figsize=(6, 2.5), dpi=120, facecolor="w", edgecolor="k")

    plt.plot(
        x_values, marker="o", markersize=2, linewidth=0.0, markevery=5, label="RNN"
    )

    if abs_value is not None:
        plt.hlines(abs_value, 0, 1000, linestyle="--", label="Exact")

    plt.xlabel(xlabel, fontsize=15)
    plt.ylabel(ylabel, fontsize=20)
    plt.title(title)
    plt.legend(loc="best")

    if not fig_name:
        plt.show()
    else:
        plt.savefig(fig_name)
        plt.show()


def plot_experiment(experiment: Experiment) -> None:
    """This function does the line plot for all energy densities in an experiment, as well
    as the transition time and convergence epoch

    """
    fig = plt.figure(
        experiment.key, figsize=(7.5, 5), dpi=120, facecolor="w", edgecolor="k"
    )
    plt.plot(experiment.densities)
    plt.vlines(
        experiment.convergence_epoch,
        -0.50,
        0.00,
        linestyle="--",
        color="green",
        label=f"Convergence Time - {experiment.convergence_epoch}",
    )
    plt.vlines(
        experiment.transition_epoch,
        -0.50,
        0.00,
        linestyle="--",
        color="black",
        label=f"Transition Time - {experiment.transition_epoch}",
    )
    plt.hlines(
        experiment.exact_value, 0, 2000, linestyle="--", color="red", label="Exact"
    )
    plt.xlabel(experiment.xlabel, fontsize=15)
    plt.ylabel(experiment.ylabel, fontsize=15)
    plt.suptitle(
        "Energy Density Over Epochs",
        fontsize="large",
        fontweight="bold",
        style="italic",
        family="monospace",
    )
    plt.title(experiment.title)
    plt.legend(loc="best")

    plt.savefig(experiment.fig_name)


def exponential_decay(original: float, time_factor: float, decay_factor: float) -> Any:
    new = original * (1 - decay_factor) ** time_factor
    return new


def edit_config(
    config: configparser.ConfigParser, edits: Iterable[Edit]
) -> configparser.ConfigParser:
    for edit in edits:
        config[edit.section][edit.key] = edit.value

    return config
