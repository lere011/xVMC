import os
import math
import time
import random
import numpy as np
import pandas as pd
import configparser
import tensorflow as tf
from tensorflow import keras


class VariationalMonteCarlo(tf.keras.Model):
    # Constructor
    def __init__(self, config: configparser.ConfigParser):
        super(VariationalMonteCarlo, self).__init__()

        """ PARAMETERS """
        self.Lx = int(config["HamiltonianParameters"]["Lx"])  # Size along x
        self.Ly = int(config["HamiltonianParameters"]["Ly"])  # Size along y
        self.V = float(config["HamiltonianParameters"]["V"])  # Van der Waals potential
        self.Omega = float(config["HamiltonianParameters"]["omega"])  # Rabi frequency
        self.delta = float(config["HamiltonianParameters"]["delta"])  # Detuning

        self.N = self.Lx * self.Ly  # Number of spins
        self.nh = int(
            config["RNN-VMCParameters"]["nh"]
        )  # Number of hidden units in the RNN
        self.ns = int(
            config["RNN-VMCParameters"]["ns"]
        )  # Number of samples to generate
        self.seed = int(
            config["RNN-VMCParameters"]["seed"]
        )  # Seed of random number generator
        self.epochs = int(config["RNN-VMCParameters"]["epochs"])  # Training epochs
        self.K = 2  # Dimension of the local Hilbert space
        self.learning_rate = float(config["RNN-VMCParameters"]["lr"])

        self.exact_energy = float(config["Diagonalization"]["exact_energy"])

        # Set the seed of the rng
        tf.random.set_seed(self.seed)
        np.random.seed(self.seed)
        random.seed(self.seed)

        self.initialize_network()

        # Generate the list of bonds for NN,NNN,NNNN on a
        # square lattice with open boundaries
        self.buildlattice()

    def initialize_network(self):
        # Optimizer
        self.optimizer = tf.optimizers.Adam(self.learning_rate, epsilon=1e-8)

        # Build the model RNN
        # RNN layer: N -> nh
        self.rnn = tf.keras.layers.GRU(
            self.nh,
            kernel_initializer="glorot_uniform",
            kernel_regularizer=tf.keras.regularizers.l2(0.001),
            return_sequences=True,
            return_state=True,
            stateful=False,
        )

        # Dense layer: nh - > K
        self.dense = tf.keras.layers.Dense(
            self.K,
            activation=tf.nn.softmax,
            kernel_regularizer=tf.keras.regularizers.l2(0.001),
        )

    @tf.function(reduce_retracing=True)
    def sample(self, nsamples):
        # Zero initialization for visible and hidden state
        inputs = 0.0 * tf.one_hot(
            tf.zeros(shape=[nsamples, 1], dtype=tf.int32), depth=self.K
        )
        hidden_state = tf.zeros(shape=[nsamples, self.nh])

        logP = tf.zeros(
            shape=[
                nsamples,
            ],
            dtype=tf.float32,
        )

        for j in range(self.N):
            # Run a single RNN cell
            rnn_output, hidden_state = self.rnn(inputs, initial_state=hidden_state)
            # Compute log probabilities
            probs = self.dense(rnn_output)
            # print(f'sample probs:\n\t{probs}')
            log_probs = tf.reshape(tf.math.log(1e-10 + probs), [nsamples, self.K])
            # Sample
            sample = tf.random.categorical(log_probs, num_samples=1)
            if j == 0:
                samples = tf.identity(sample)
            else:
                samples = tf.concat([samples, sample], axis=1)
            # Feed result to the next cell
            inputs = tf.one_hot(sample, depth=self.K)
            add = tf.reduce_sum(
                log_probs * tf.reshape(inputs, (nsamples, self.K)), axis=1
            )

            logP = logP + tf.reduce_sum(
                log_probs * tf.reshape(inputs, (nsamples, self.K)), axis=1
            )

        return samples, logP

    @tf.function(reduce_retracing=True)
    def logpsi(self, samples):
        # Shift data
        num_samples = tf.shape(samples)[0]
        data = tf.one_hot(samples[:, 0 : self.N - 1], depth=self.K)

        x0 = 0.0 * tf.one_hot(
            tf.zeros(shape=[num_samples, 1], dtype=tf.int32), depth=self.K
        )
        inputs = tf.concat([x0, data], axis=1)

        hidden_state = tf.zeros(shape=[num_samples, self.nh])
        rnn_output, _ = self.rnn(inputs, initial_state=hidden_state)
        # print(rnn_output)
        probs = self.dense(rnn_output)
        # print(probs)

        log_probs = tf.math.log(1e-10 + probs)
        logP = tf.reduce_sum(
            tf.multiply(log_probs, tf.one_hot(samples, depth=self.K)), axis=2
        )
        logP = 0.5 * tf.reduce_sum(logP, axis=1)

        return logP

    # @tf.function
    def localenergy(self, samples, logpsi, df=None):
        eloc = tf.zeros(shape=[tf.shape(samples)[0]], dtype=tf.float32)

        # Chemical potential
        for j in range(self.N):
            eloc += -self.delta * tf.cast(samples[:, j], tf.float32)

        # NN
        if df is not None:
            for i in range(len(df)):
                p1 = df.loc[i, "Index 1"]
                p2 = df.loc[i, "Index 2"]
                factor = df.loc[i, "Multiplier"]

                eloc += (
                    self.V
                    * factor
                    * tf.cast(samples[:, p1] * samples[:, p2], tf.float32)
                )

        else:
            for n in range(len(self.nn)):
                eloc += self.V * tf.cast(
                    samples[:, self.nn[n][0]] * samples[:, self.nn[n][1]], tf.float32
                )
            for n in range(len(self.nnn)):
                eloc += (self.V / 8.0) * tf.cast(
                    samples[:, self.nnn[n][0]] * samples[:, self.nnn[n][1]], tf.float32
                )
            for n in range(len(self.nnnn)):
                eloc += (self.V / 64.0) * tf.cast(
                    samples[:, self.nnnn[n][0]] * samples[:, self.nnnn[n][1]],
                    tf.float32,
                )

        # Off-diagonal part
        for j in range(self.N):
            flip_samples = np.copy(samples)
            flip_samples[:, j] = 1 - flip_samples[:, j]
            flip_logpsi = self.logpsi(flip_samples)
            eloc += -0.5 * self.Omega * tf.math.exp(flip_logpsi - logpsi)

        return eloc

    """ Generate the square lattice structures """

    def coord_to_site(self, x, y):
        return self.Ly * x + y

    def buildlattice(self):
        self.nn = []
        self.nnn = []
        self.nnnn = []
        for x in range(self.Lx):
            for y in range(self.Ly - 1):
                self.nn.append([self.coord_to_site(x, y), self.coord_to_site(x, y + 1)])
        for y in range(self.Ly):
            for x in range(self.Lx - 1):
                self.nn.append([self.coord_to_site(x, y), self.coord_to_site(x + 1, y)])

        for y in range(self.Ly - 1):
            for x in range(self.Lx - 1):
                self.nnn.append(
                    [self.coord_to_site(x, y), self.coord_to_site(x + 1, y + 1)]
                )
                self.nnn.append(
                    [self.coord_to_site(x + 1, y), self.coord_to_site(x, y + 1)]
                )

        for y in range(self.Ly):
            for x in range(self.Lx - 2):
                self.nnnn.append(
                    [self.coord_to_site(x, y), self.coord_to_site(x + 2, y)]
                )
        for y in range(self.Ly - 2):
            for x in range(self.Lx):
                self.nnnn.append(
                    [self.coord_to_site(x, y), self.coord_to_site(x, y + 2)]
                )
