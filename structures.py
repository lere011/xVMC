import csv
import dataclasses
from dataclasses import dataclass, field
from collections.abc import Iterable
from typing import List, Union, Dict, TypeVar
from enum import Enum
import numpy as np


TypeScale = TypeVar("TypeScale", bound="NormScaler")


@dataclass
class NormScaler:
    minimum: Union[int, float]
    maximum: Union[int, float]
    current: Union[int, float]

    def scale(self: TypeScale) -> float:
        scaled = (self.current - self.minimum) / (self.maximum - self.minimum)
        return scaled


@dataclass
class Edit:
    """The entire configuration for the package is held in a .ini file

    That file can be edited by passing this class to the edit_config() function.
    """

    section: str
    key: str
    value: str


@dataclass
class Combination:
    """The class holds the combination of cofiguration values that is used to vary one experiment
    from the other

    """

    hidden_units: int
    sample_percent: float
    transition_time: float


@dataclass
class Experiment:
    """This class holds all the data that is required for plotting experiment chart

    This class is expected to be used with the plot_experiment() function.
    """

    key: int
    densities: Iterable[float]
    exact_value: float
    title: str
    fig_name: str
    transition_epoch: int
    convergence_epoch: int
    xlabel: str = "steps"
    ylabel: str = "$\\langle H \\rangle/N$"


@dataclass
class ExperimentLog:
    """This class holds all the data that is expected to be logged for individual experiments"""

    covergence_time: int
    energy_density_half_time: float
    energy_density_full_time: float
    transition_time: int
    hidden_units: int
    amt_training_sample: int


@dataclass
class DecayParams:
    """This holds the exoected parameters for computing decay of a value"""

    original: Union[int, float, None] = 0.0
    decay_constant: Union[float, None] = 0.0
    time_factor: Union[int, None] = 0
    slope: Union[int, float, None] = 0.0
    bias: Union[int, float, None] = 0.0


TypeLog = TypeVar("TypeLog", bound="Log")  # Type for class Log self object


@dataclass
class Log:
    """This class holds all the logs for all experiments"""

    log: List[Dict[str, Union[int, float]]] = field(default_factory=list)

    def __post_init__(self: TypeLog) -> None:
        if not self.log:
            self.log = []

    def update_with(self: TypeLog, exp_log: ExperimentLog) -> None:
        self.log.append(dataclasses.asdict(exp_log))

    def to_csv(self: TypeLog, filename: str) -> None:
        with open(filename, "w", newline="") as f:
            writer = csv.DictWriter(f, fieldnames=self.log[0].keys())
            writer.writeheader()
            writer.writerows(self.log)


TypeDF = TypeVar("TypeDF", bound="DecayFunctions")


class DecayFunctions:
    """Different exponential decay functions
    A: original value
    k: decay constant
    x: time factor
    """

    # original: Union[int, float], decay_constant: float, time_factor: int,

    def __init__(self: TypeDF, decay_type: int = 1, reg: float = 1e-8):
        # self.original = original # initial value
        # self.decay_constant = decay_constant
        # self.time_factor = time_factor
        self.reg = reg

        match decay_type:
            case 1:
                self.decay_func = self.type_one
            case 2:
                self.decay_func = self.type_two
            case 3:
                self.decay_func = self.type_three
            case 4:
                self.decay_func = self.linear_decay

    def type_one(self: TypeDF, params: DecayParams) -> float:
        """Simple Exponential Decay

        y = A * e^(-kx)
        """
        f = (params.decay_constant * params.time_factor) + self.reg
        new = params.original * np.exp(-f)
        return new

    def type_two(self: TypeDF, params: DecayParams) -> float:
        """Exponential Decay with Decay Factor

        y = A * (1 - k) ^(x)
        """
        new = params.original * (1 - params.decay_constant) ** params.time_factor
        return new
        ...

    def type_three(self: TypeDF, params: DecayParams) -> float:
        """Exponential Decay with Time Constant
        y = A * e^(-x/τ)
        """
        dc = params.decay_constant + self.reg
        time_constant = 1 / dc
        f = (params.time_factor / time_constant) + self.reg
        new = params.original * np.exp(-f)
        return new

    def type_four(self: TypeDF, params: DecayParams) -> float:
        """Exponential Decay with Power Law

        y = A * x^(-k)
        """
        # f = params.decay_constant + self.reg
        # time_factor = params.time_factor + self.reg
        new = params.original * (params.time_factor + self.reg) ** (
            -(params.decay_constant + self.reg)
        )
        return new

    def type_five(self: TypeDF, params: DecayParams) -> None:
        """Exponential Decay with Multiple Decay Constants:
        y = A * e^(-k₁x) + B * e^(-k₂x)
        """
        raise NotImplemented

    def linear_decay(self: TypeDF, params: DecayParams) -> float:
        """Linear relationship with negative slope

        y = mx + b
        """
        new = params.time_factor * (-params.slope) + params.bias
        return new


TypeDecay = TypeVar("TypeDecay", bound="Decay")


class Decay(Enum):
    """Class to represent the different variants of exponential decay functions"""

    TYPE1 = 1
    TYPE2 = 2
    TYPE3 = 3
    TYPE4 = 4
    TYPE5 = 5

    def decay_func(cls: TypeDecay):
        d_class = DecayFunctions(cls.value)
        d_func = d_class.decay_func
        return d_func
