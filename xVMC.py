import os
import logging
import numpy as np
import configparser
import numpy.typing as npt
import matplotlib.pyplot as plt  # type: ignore
from typing import TypeVar, List, Union

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # type: ignore
import tensorflow as tf  # type: ignore

from helpers import get_all_interactions, exponential_decay
from decorators import time_wrapper, execute_wrapper
from wavefunction import VariationalMonteCarlo
from structures import Decay, DecayParams, NormScaler

TypeXVMC = TypeVar("TypeXVMC", bound="xVMC")


class xVMC(VariationalMonteCarlo):
    def __init__(
        self: TypeXVMC,
        training_data: npt.NDArray[np.int32],
        config: configparser.ConfigParser,
    ):
        super(xVMC, self).__init__(config)
        self.data = training_data
        self.energy_densities: List[float] = []
        self.data_based_energy_densities: List[float] = []
        self.mixed_energy_densities: List[float] = []
        self.exp_energy_densities: List[float] = []
        self.checkpoint = int(config["RNN-VMCParameters"]["checkpoint"])
        self.interactions = get_all_interactions(self.Lx)
        self.batch_size = int(config["xVMC"]["num_batches"])
        self.num_data_samples = int(config["xVMC"]["num_samples"])
        self.hqmc = float(config["xVMC"]["hqmc"])
        self.train_split = float(config["xVMC"]["train_split"])
        self.min_epoch = 1

        logging.info("xVMC class instantiated")

    def reset_network(self: TypeXVMC) -> None:
        tf.keras.backend.clear_session()

    @tf.function(reduce_retracing=True)
    def get_loss(self: TypeXVMC) -> float:
        num_samples = tf.shape(self.data)[0]
        logP = 2 * self.logpsi(self.data)
        loss = -tf.reduce_sum(logP) / float(num_samples)

        return loss

    @tf.function(reduce_retracing=True)
    def get_batch_loss(self, batch_data):
        num_samples = tf.shape(batch_data)[0]
        logP = 2 * self.logpsi(batch_data)
        loss = -tf.reduce_sum(logP) / float(num_samples)

        return loss

    def compute_metric(self, hold_density_append=False) -> float:
        samples, _ = self.sample(
            self.num_data_samples
        )  # check here if there is wahala... changed ns to num_data_samples
        logpsi = self.logpsi(samples)

        local_e = self.localenergy(samples, logpsi, self.interactions)
        energy_density = np.mean(local_e.numpy()) / self.N

        return energy_density

    def plot_metric(self, plot_fig: str = "metrics.png") -> None:
        fig = plt.figure(1, figsize=(6, 2.5), dpi=120, facecolor="w", edgecolor="k")

        plt.plot(
            self.energy_densities,
            marker="o",
            markersize=2,
            linewidth=0.0,
            markevery=5,
            label="RNN",
        )
        plt.hlines(self.exact_energy, 0, 1000, linestyle="--", label="Exact")

        plt.xlabel("Step", fontsize=15)
        plt.ylabel("$\\langle H \\rangle/N$", fontsize=20)
        plt.title("Energy Density Over Epochs")
        plt.legend(loc="best")
        plt.savefig(plot_fig)
        plt.show()

    @time_wrapper
    def train(self, data_train: bool = True):
        trainer = self.data_epoch_train if data_train else self.epoch_train
        message = (
            "Using data-backed vmc training"
            if data_train
            else "Using vanilla VMC training"
        )

        print(f"{message}...")

        for epoch in range(1, self.epochs + 1):
            energy_density = trainer()

            if epoch % 100 == 0:
                print(f"Epoch number: {epoch}, energy: {energy_density}")

    @time_wrapper
    def combined_train(self, train_split=0.3):
        if not isinstance(train_split, (int, float)):
            raise TypeError("train_split should be a number")

        if train_split < 0 or train_split > 1:
            raise ValueError("train_split value should be between 0 and 1")

        hold_density_append = True
        train_split = int(train_split * self.epochs)

        for epoch in range(self.min_epoch, self.epochs + 1):
            if epoch <= train_split:
                tm = "data-backed vmc"
                energy_density = self.data_epoch_train(hold_density_append)
            else:
                tm = "vanilla vmc"
                energy_density = self.epoch_train(hold_density_append)

            self.mixed_energy_densities.append(energy_density)

            if epoch % 100 == 0:
                print(f"{tm} - Epoch number: {epoch}, energy: {energy_density}")

    @time_wrapper
    def decay_train(
        self,
        decay_factor: float = 0.15,
        alpha: float = 0.75,
        decay_type: Decay = Decay.TYPE1,
        slope: Union[float, None] = None,
        bias: Union[float, None] = None,
    ):
        # set the minimum and maximum epsilon for the network so that we can do so normalization later...
        # note that epsilon is what is referred to as alpha here... it is the weighting factor for
        # data-driven loss function... beta is the weight factor for hamiltonian-driven loss function

        self.current_epsilon = alpha

        logging.info("Using decaying combination")
        eps = []

        # run through the training per epoch
        for epoch in range(self.min_epoch, self.epochs + 1):
            if epoch == self.min_epoch:
                self.initialize_epsilon(
                    alpha=alpha,
                    decay_type=decay_type,
                    decay_factor=decay_factor,
                    slope=slope,
                    bias=bias,
                )
                beta = 1 - alpha
            else:
                alpha, beta = self.get_loss_coefficients(
                    alpha, epoch, decay_type, decay_factor, slope=slope, bias=bias
                )

            eps.append((alpha, beta))

            samples, _ = self.sample(self.ns)

            with tf.GradientTape() as tape:
                logpsi = self.logpsi(samples)

                with tape.stop_recording():
                    eloc = self.localenergy(samples, logpsi, self.interactions)
                    Eo = tf.reduce_mean(eloc)

                hamiltonian_loss = tf.reduce_mean(
                    2.0 * tf.multiply(logpsi, tf.stop_gradient(eloc))
                    - 2.0 * tf.stop_gradient(Eo) * logpsi
                )
                data_loss = self.get_loss()

                loss = (alpha * data_loss) + (beta * hamiltonian_loss)

            # Compute the gradients
            gradients = tape.gradient(loss, self.trainable_variables)

            # Update the parameters
            self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))

            ave_energy = np.mean(eloc.numpy())
            energy_density = ave_energy / float(self.N)
            self.exp_energy_densities.append(energy_density)

            if epoch % 100 == 0:
                logging.info(f"Epoch number: {epoch}, energy: {energy_density}")

        return eps

    def scale_epoch(self, epoch: int) -> float:
        scaled = NormScaler(
            minimum=self.min_epoch, maximum=self.epochs, current=epoch
        ).scale()

        return scaled

    def get_loss_coefficients(
        self,
        alpha: Union[int, float],
        time_factor: int,
        decay_type: Decay,
        decay_factor: Union[float, None] = None,
        slope: Union[float, None] = None,
        bias: Union[float, None] = None,
    ) -> tuple:
        """Compute alpha and beta to be used decay_train method

        alpha is the epsilon multiplication factor for data-driven loss function
        """

        params = DecayParams(
            original=self.current_epsilon,
            decay_constant=decay_factor,
            time_factor=time_factor,
            slope=slope,
            bias=bias,
        )

        decay_func = decay_type.decay_func()
        self.current_epsilon = decay_func(params)

        # normalize the new alpha
        scaled_alpha = NormScaler(
            minimum=self.min_epsilon,
            maximum=self.max_epsilon,
            current=self.current_epsilon,
        ).scale()

        # we need alpha and beta to be normalized
        new_beta = 1 - scaled_alpha

        return scaled_alpha, new_beta

    def initialize_epsilon(
        self,
        alpha: Union[int, float],
        decay_type: Decay,
        decay_factor: Union[float, None] = None,
        slope: Union[float, None] = None,
        bias: Union[float, None] = None,
    ) -> None:
        # get decay function
        decay_func = decay_type.decay_func()

        # start with minimum epsilon
        min_params = DecayParams(
            original=alpha,
            decay_constant=decay_factor,
            time_factor=self.min_epoch,
            slope=slope,
            bias=bias,
        )
        self.min_epsilon = decay_func(min_params)

        # then maximum epsilon
        max_params = DecayParams(
            original=alpha,
            decay_constant=decay_factor,
            time_factor=self.epochs,
            slope=slope,
            bias=bias,
        )
        self.max_epsilon = decay_func(max_params)

    def data_epoch_train(self, hold_density_append=False):
        train_dataset = (
            tf.data.Dataset.from_tensor_slices(self.data)
            .shuffle(buffer_size=1024)
            .batch(self.batch_size)
        )

        for _, dataset in enumerate(train_dataset):
            with tf.GradientTape() as tape:
                loss = self.get_batch_loss(dataset)

            # compute metric from generated samples
            energy_density = self.compute_metric(hold_density_append)

            # Compute the gradients
            gradients = tape.gradient(loss, self.trainable_variables)

            # Update the parameters
            self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))

        if hold_density_append == False:
            self.data_based_energy_densities.append(energy_density)

        return energy_density

    def epoch_train(self, hold_density_append=False):
        samples, _ = self.sample(self.ns)

        # Evaluate the loss function in AD mode
        with tf.GradientTape() as tape:
            logpsi = self.logpsi(samples)

            with tape.stop_recording():
                eloc = self.localenergy(samples, logpsi, self.interactions)
                Eo = tf.reduce_mean(eloc)

            loss = tf.reduce_mean(
                2.0 * tf.multiply(logpsi, tf.stop_gradient(eloc))
                - 2.0 * tf.stop_gradient(Eo) * logpsi
            )

        # Compute the gradients
        gradients = tape.gradient(loss, self.trainable_variables)

        # Update the parameters
        self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))

        ave_energy = np.mean(eloc.numpy())
        energy_density = ave_energy / float(self.N)

        if hold_density_append == False:
            self.energy_densities.append(energy_density)

        return energy_density
