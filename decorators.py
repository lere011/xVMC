import time
import logging
from functools import wraps
from typing import Callable, Any


def time_wrapper(func: Callable[[Any], Any]) -> Callable[[Any], Any]:
    @wraps(func)
    def timeit_wrapper(*args: Any, **kwargs: Any) -> Any:
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time

        logging.info(
            f"Function {func.__name__}{args} {kwargs} Took {total_time:.4f} seconds"
        )
        return result

    return timeit_wrapper


def execute_wrapper(**params: Any) -> Callable[[Any], Any]:
    def execute(func: Callable[[Any], Any]) -> Callable[[Any], None]:
        @wraps(func)
        def executor(*args: Any, **kwargs: Any) -> None:
            for epoch in range(1, params["epochs"] + 1):
                result = func(*args, **kwargs)

                if epoch % params["checkpoint"] == 0:
                    logging.info(f"Epoch number: {epoch}, energy: {result}")

        return executor

    return execute
